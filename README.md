# caozha-pinyin（中文转拼音）

#### 介绍
caozha-pinyin，是一个用原生PHP写的中文转拼音的模块。中文转换拼音，支持带或不带声调，支持把ü替换为v等任意字符，支持过滤标点符号，支持使用任意符号做连接符，同时支持json、jsonp、text、xml、js等多种输出格式。

## 重要说明

## 本项目已经迁移，请您到以下的新网址去下载和使用：


 **Gitee（国内仓库）：** https://gitee.com/dengzhenhua/caozha-pinyin

 **GitHub（国外仓库）：** https://github.com/dengcao/caozha-pinyin

